package com.github.axet.torrentclient.dialogs;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.github.axet.androidlibrary.net.HttpClient;
import com.github.axet.androidlibrary.widgets.DialogFragmentCompat;
import com.github.axet.androidlibrary.widgets.ErrorDialog;
import com.github.axet.androidlibrary.widgets.WebViewCustom;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;

import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLDecoder;
import java.nio.charset.Charset;

public class OpenIntentDialogFragment extends DialogFragmentCompat {

    Handler handler = new Handler();
    Thread t;

    @Override
    public void onStart() {
        super.onStart();

        final MainActivity main = (MainActivity) getActivity();

        t = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    openURL(getArguments().getString("url"));
                } catch (final RuntimeException e) {
                    ErrorDialog.Post(main, e);
                }
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (main.isFinishing())
                            return;
                        dismissAllowingStateLoss(); //  Can not perform this action after onSaveInstanceState
                    }
                });
            }
        }, "Delayed Intent");
        t.start();
    }

    public void openURL(final String str) {
        final MainActivity activity = (MainActivity) getActivity();
        if (activity == null) // when app was destoryed
            return;

        if (str.startsWith(Storage.SCHEME_MAGNET)) {
            handler.post(new Runnable() {
                @Override
                public void run() {
                    if (activity.isFinishing())
                        return;
                    activity.addMagnet(str);
                }
            });
            return;
        }

        if (str.startsWith(ContentResolver.SCHEME_CONTENT)) {
            try {
                Uri uri = Uri.parse(str);
                final byte[] buf = IOUtils.toByteArray(activity.getContentResolver().openInputStream(uri));
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activity.isFinishing())
                            return;
                        activity.addTorrentFromBytes(buf);
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return;
        }

        if (str.startsWith(WebViewCustom.SCHEME_HTTP)) {
            try {
                URL url = new URL(str);
                URLConnection conn = url.openConnection();
                conn.setConnectTimeout(HttpClient.CONNECTION_TIMEOUT);
                final byte[] buf = IOUtils.toByteArray(conn);
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activity.isFinishing())
                            return;
                        activity.addTorrentFromBytes(buf);
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return;
        }

        if (str.startsWith(ContentResolver.SCHEME_FILE)) {
            Uri uri = Uri.parse(str);
            try {
                String path = uri.getEncodedPath();
                final String s = URLDecoder.decode(path, Charset.defaultCharset().displayName());
                final byte[] buf = IOUtils.toByteArray(new FileInputStream(new File(s)));
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activity.isFinishing())
                            return;
                        activity.addTorrentFromBytes(buf);
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
            return;
        }

        // .torrent?
        if (new File(str).exists()) {
            try {
                final byte[] buf = IOUtils.toByteArray(new FileInputStream(new File(str)));
                handler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (activity.isFinishing())
                            return;
                        activity.addTorrentFromBytes(buf);
                    }
                });
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    @Override
    public View createView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ProgressBar view = new ProgressBar(inflater.getContext());
        view.setIndeterminate(true);

        // wait until torrent loaded
        setCancelable(false);

        //getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return view;
    }
}
