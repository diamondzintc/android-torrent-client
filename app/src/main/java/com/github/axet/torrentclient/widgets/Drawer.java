package com.github.axet.torrentclient.widgets;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.preferences.AboutPreferenceCompat;
import com.github.axet.androidlibrary.widgets.OpenChoicer;
import com.github.axet.androidlibrary.widgets.ThemeUtils;
import com.github.axet.androidlibrary.widgets.Toast;
import com.github.axet.androidlibrary.widgets.UnreadCountDrawable;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.navigators.Torrents;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import libtorrent.Libtorrent;

public class Drawer implements UnreadCountDrawable.UnreadCount {
    public static final String TAG = Drawer.class.getSimpleName();

    static final long INFO_MANUAL_REFRESH = 5 * AlarmManager.SEC1; // prevent refresh if button hit often then 5 seconds
    static final long INFO_AUTO_REFRESH = 5 * AlarmManager.MIN1; // ping external port on drawer open not often then 5 minutes

    Context context;
    MainActivity main;
    Handler handler = new Handler();
    OpenChoicer choicer;

    NavigationView navigationView;
    View navigationHeader;
    DrawerLayout drawer;
    UnreadCountDrawable unread;

    Thread infoThread;
    List<String> infoOld;
    Boolean infoPort;
    long infoTime; // last time checked

    public static boolean signatureCheck(Context context, int[] dd) {
        for (int d : dd) {
            if (signatureCheck(context, d))
                return true;
        }
        return false;
    }

    public static boolean signatureCheck(Context context, int d) {
        try {
            PackageManager pm = context.getPackageManager();
            Signature[] ss = pm.getPackageInfo(context.getPackageName(), PackageManager.GET_SIGNATURES).signatures;
            for (Signature s : ss) {
                int hash = s.hashCode();
                if (d == hash)
                    return true;
            }
        } catch (PackageManager.NameNotFoundException e) {
        }
        return false;
    }

    public Drawer(final MainActivity main, final Toolbar toolbar, final ActionBar bar) {
        this.context = main;
        this.main = main;

        navigationView = (NavigationView) main.findViewById(R.id.nav_view); // inflater.inflate(R.layout.nav_header_main, null, false);
        navigationHeader = navigationView.getHeaderView(0);

        drawer = (DrawerLayout) main.findViewById(R.id.drawer_layout);

        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                return onItemClick(item);
            }
        });

        Drawable navigationIcon = toolbar.getNavigationIcon();
        if (navigationIcon == null) {
            ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(main, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                public void onDrawerClosed(View view) {
                    main.supportInvalidateOptionsMenu();
                    updateManager();
                }

                public void onDrawerOpened(View drawerView) {
                    main.supportInvalidateOptionsMenu();
                    updateManager();
                }
            };
            bar.setDisplayHomeAsUpEnabled(true);
            toggle.setDrawerIndicatorEnabled(true);
            drawer.addDrawerListener(toggle);
            toggle.syncState();
            navigationIcon = toolbar.getNavigationIcon();
        }
        unread = new UnreadCountDrawable(context, navigationIcon, Drawer.this);
        unread.setPadding(ThemeUtils.dp2px(main, 15));
        toolbar.setNavigationIcon(unread);

        TextView ver = (TextView) navigationHeader.findViewById(R.id.nav_version);
        AboutPreferenceCompat.setVersion(ver);
    }

    public void close() {
    }

    public boolean isDrawerOpen() {
        return drawer.isDrawerOpen(Gravity.LEFT);
    }

    public void openDrawer() {
        drawer.openDrawer(Gravity.LEFT);
    }

    public void closeDrawer() {
        drawer.closeDrawer(Gravity.LEFT);
    }

    public void updateUnread() {
        unread.update();
    }

    public void updateManager() {
        Menu menu = navigationView.getMenu();
        menu.clear();

        MenuItem item;

        final Torrents torrents = main.getTorrents();
        if (torrents == null) {
            item = menu.add("");
            ProgressBar progressBar = new ProgressBar(context);
            MenuItemCompat.setActionView(item, progressBar);
            return;
        }

        item = menu.add(Menu.NONE, R.id.nav_torrents, Menu.NONE, R.string.torrents);
        item.setIcon(new UnreadCountDrawable(context, R.drawable.ic_storage_black_24dp, torrents));
        item.setChecked(torrents.navfilter == Torrents.Filter.FILTER_ALL);

        item = menu.add(Menu.NONE, R.id.nav_downloading, Menu.NONE, R.string.status_downloading);
        item.setIcon(R.drawable.ic_drag_handle_black_24dp);
        item.setChecked(torrents.navfilter == Torrents.Filter.FILTER_DOWNLOADING);

        item = menu.add(Menu.NONE, R.id.nav_seeding, Menu.NONE, R.string.status_seeding);
        item.setIcon(R.drawable.ic_drag_handle_black_24dp);
        item.setChecked(torrents.navfilter == Torrents.Filter.FILTER_SEEDING);

        item = menu.add(Menu.NONE, R.id.nav_queued, Menu.NONE, R.string.status_queue);
        item.setIcon(R.drawable.ic_drag_handle_black_24dp);
        item.setChecked(torrents.navfilter == Torrents.Filter.FILTER_QUEUED);
    }

    public void updateHeader() {
        ArrayList<String> info = new ArrayList<>();
        long c = Libtorrent.portCount();
        for (long i = 0; i < c; i++)
            info.add(Libtorrent.port(i));

        StringBuilder str = new StringBuilder();
        for (String ip : info) {
            str.append(ip);
            str.append("\n");
        }
        final String ss = str.toString().trim();

        TextView textView = (TextView) navigationHeader.findViewById(R.id.torrent_ip);
        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(context, ss, Toast.LENGTH_SHORT).show();
            }
        });
        textView.setText(ss);

        View portButton = navigationHeader.findViewById(R.id.torrent_port_button);
        ImageView portIcon = (ImageView) navigationHeader.findViewById(R.id.torrent_port_icon);
        TextView port = (TextView) navigationHeader.findViewById(R.id.torrent_port_text);

        portButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long time = System.currentTimeMillis();
                if (infoTime + INFO_MANUAL_REFRESH < time) {
                    infoTime = time;
                    infoOld = null;
                }
            }
        });

        long time = System.currentTimeMillis();
        if (infoTime + INFO_AUTO_REFRESH < time) {
            infoTime = time;
            infoOld = null;
        }

        if (infoOld == null || !Arrays.equals(info.toArray(), infoOld.toArray())) {
            if (isDrawerOpen()) { // only probe port when drawer is open
                if (infoThread != null)
                    return;
                infoOld = info;
                infoThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            final boolean b = Libtorrent.portCheck();
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = b;
                                    infoThread = null;
                                }
                            });
                        } catch (Exception e) {
                            handler.post(new Runnable() {
                                @Override
                                public void run() {
                                    infoPort = null;
                                    infoThread = null;
                                }
                            });
                        }
                    }
                }, "Port Check");
                infoThread.start();
                infoPort = false;
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_checking);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        } else {
            if (infoPort == null) {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_down);
            } else if (infoPort) {
                portIcon.setImageResource(R.drawable.port_ok);
                port.setText(R.string.port_open);
            } else {
                portIcon.setImageResource(R.drawable.port_no);
                port.setText(R.string.port_closed);
            }
        }
    }

    public boolean onItemClick(MenuItem item) {
        long id = item.getItemId();

        if (id == R.id.nav_torrents) {
            main.openTorrents(Torrents.Filter.FILTER_ALL);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_downloading) {
            main.openTorrents(Torrents.Filter.FILTER_DOWNLOADING);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_seeding) {
            main.openTorrents(Torrents.Filter.FILTER_SEEDING);
            closeDrawer();
            return true;
        }

        if (id == R.id.nav_queued) {
            main.openTorrents(Torrents.Filter.FILTER_QUEUED);
            closeDrawer();
            return true;
        }

        return true;
    }

    @Override
    public int getUnreadCount() {
        int count = 0;
        Torrents torrents = main.getTorrents();
        if (torrents != null)
            count += torrents.getUnreadCount();
        return count;
    }

    @TargetApi(21)
    public void onActivityResult(int resultCode, Intent data) {
        choicer.onActivityResult(resultCode, data);
    }

    public void onRequestPermissionsResult(String[] permissions, int[] grantResults) {
        choicer.onRequestPermissionsResult(permissions, grantResults);
    }
}
