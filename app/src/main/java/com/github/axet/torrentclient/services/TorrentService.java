package com.github.axet.torrentclient.services;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v7.preference.PreferenceManager;
import android.util.Log;
import android.view.View;

import com.github.axet.androidlibrary.app.AlarmManager;
import com.github.axet.androidlibrary.app.NotificationManagerCompat;
import com.github.axet.androidlibrary.app.ProximityShader;
import com.github.axet.androidlibrary.preferences.OptimizationPreferenceCompat;
import com.github.axet.androidlibrary.services.PersistentService;
import com.github.axet.androidlibrary.sound.Headset;
import com.github.axet.androidlibrary.widgets.CacheImagesAdapter;
import com.github.axet.androidlibrary.widgets.RemoteNotificationCompat;
import com.github.axet.torrentclient.R;
import com.github.axet.torrentclient.activities.BootActivity;
import com.github.axet.torrentclient.activities.MainActivity;
import com.github.axet.torrentclient.app.Storage;
import com.github.axet.torrentclient.app.TorrentApplication;
import com.github.axet.torrentclient.app.TorrentPlayer;

public class TorrentService extends PersistentService {
    public static final String TAG = TorrentService.class.getSimpleName();

    public static final int NOTIFICATION_TORRENT_ICON = 1;
    public static final int NOTIFICATION_DOWNLOAD_ICON = 10; // 10 + number of torrents

    public static String TITLE = "TITLE";
    public static String PLAYER = "player";
    public static String PLAYING = "playing";

    public static String UPDATE_NOTIFY = TorrentService.class.getCanonicalName() + ".UPDATE_NOTIFY";
    public static String SHOW_ACTIVITY = TorrentService.class.getCanonicalName() + ".SHOW_ACTIVITY";
    public static String PAUSE_BUTTON = TorrentService.class.getCanonicalName() + ".PAUSE_BUTTON";

    static {
        OptimizationPreferenceCompat.REFRESH = 5 * AlarmManager.MIN1;
    }

    TorrentReceiver receiver;
    Headset headset;
    PendingIntent pause;

    public static void startService(Context context, String title) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(TorrentApplication.PREFERENCE_RUN, true);
        edit.commit();
        Intent i = new Intent(context, TorrentService.class).setAction(UPDATE_NOTIFY).putExtra(TITLE, title);
        OptimizationPreferenceCompat.startService(context, i);
    }

    public static Intent createIntent(String title, String player, boolean playing) {
        return new Intent(UPDATE_NOTIFY)
                .putExtra(TITLE, title)
                .putExtra(PLAYER, player)
                .putExtra(PLAYING, playing);
    }

    public static void updateNotify(Context context, String title, String player, boolean playing) {
        context.sendBroadcast(createIntent(title, player, playing));
    }

    public static void stopService(Context context) {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor edit = shared.edit();
        edit.putBoolean(TorrentApplication.PREFERENCE_RUN, false);
        edit.commit();
        context.stopService(new Intent(context, TorrentService.class));
    }

    @SuppressLint("RestrictedApi")
    public static void notifyDone(Context context, Storage.Torrent t) {
        RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notifictaion);

        PendingIntent main = PendingIntent.getService(context, 0,
                new Intent(context, TorrentService.class).setAction(TorrentService.SHOW_ACTIVITY),
                PendingIntent.FLAG_UPDATE_CURRENT);

        builder.setViewVisibility(R.id.notification_player, View.GONE);

        Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        builder.setTheme(TorrentApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                .setChannel(TorrentApplication.from(context).channelDownloads)
                .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                .setTitle(context.getString(R.string.app_name))
                .setText(t.name())
                .setMainIntent(main)
                .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                .setSmallIcon(R.drawable.ic_downloaded)
                .setAutoCancel(true)
                .setLights(Color.BLUE, 500, 500)
                .setSound(alarmSound);

        NotificationManagerCompat nm = NotificationManagerCompat.from(context);
        nm.notify(String.valueOf(System.currentTimeMillis()), NOTIFICATION_DOWNLOAD_ICON, builder.build());
    }

    public class TorrentReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            String a = intent.getAction();
            if (a == null)
                return;
            if (a.equals(UPDATE_NOTIFY)) {
                optimization.icon.updateIcon(intent);
                return;
            }
            Log.d(TAG, "TorrentReceiver " + intent); // skip freq update log messages
            Headset.handleIntent(headset, intent);
        }
    }

    public TorrentService() {
    }

    boolean isRunning() {
        final SharedPreferences shared = PreferenceManager.getDefaultSharedPreferences(this);
        return shared.getBoolean(TorrentApplication.PREFERENCE_RUN, false);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.d(TAG, "onCreate");

        receiver = new TorrentReceiver();
        registerReceiver(receiver, new IntentFilter(UPDATE_NOTIFY));

        if (!isRunning()) {
            stopSelf();
            return;
        }
    }

    @Override
    public void onCreateOptimization() {
        optimization = new OptimizationPreferenceCompat.ServiceReceiver(this, NOTIFICATION_TORRENT_ICON, TorrentApplication.PREFERENCE_OPTIMIZATION, TorrentApplication.PREFERENCE_NEXT) {
            @Override
            public void check() {
                ping();
            }

            @SuppressLint("RestrictedApi")
            @Override
            public Notification build(Intent intent) {
                String title = intent.getStringExtra(TITLE);
                String player = intent.getStringExtra(PLAYER);
                boolean playing = intent.getBooleanExtra(PLAYING, false);

                PendingIntent main = PendingIntent.getService(context, 0,
                        new Intent(context, TorrentService.class).setAction(SHOW_ACTIVITY),
                        PendingIntent.FLAG_UPDATE_CURRENT);

                // PendingIntent pe = PendingIntent.getService(this, 0,
                //        new Intent(this, TorrentService.class).setAction(PAUSE_BUTTON),
                //        PendingIntent.FLAG_UPDATE_CURRENT);

                pause = PendingIntent.getBroadcast(context, 0,
                        new Intent(TorrentPlayer.PLAYER_PAUSE),
                        PendingIntent.FLAG_UPDATE_CURRENT);

                RemoteNotificationCompat.Builder builder = new RemoteNotificationCompat.Builder(context, R.layout.notifictaion);

                // boolean pause = false; // getStorage().isPause();
                // view.setOnClickPendingIntent(R.id.notification_pause, pe);
                // view.setImageViewResource(R.id.notification_pause, pause ? R.drawable.play : R.drawable.pause);

                if (player == null || player.isEmpty()) {
                    builder.setViewVisibility(R.id.notification_play, View.GONE);
                    builder.setViewVisibility(R.id.notification_playing, View.GONE);
                    headset(false, playing);
                } else {
                    builder.setViewVisibility(R.id.notification_play, View.VISIBLE);
                    builder.setViewVisibility(R.id.notification_playing, View.VISIBLE);
                    builder.setTextViewText(R.id.notification_play, player);
                    builder.setImageViewResource(R.id.notification_playing, playing ? R.drawable.ic_pause_24dp : R.drawable.ic_play_arrow_black_24dp);
                    headset(true, playing);
                }
                builder.setOnClickPendingIntent(R.id.notification_playing, pause);

                builder.setTheme(TorrentApplication.getTheme(context, R.style.AppThemeLight, R.style.AppThemeDark))
                        .setChannel(TorrentApplication.from(context).channelStatus)
                        .setMainIntent(main)
                        .setTitle(getString(R.string.app_name))
                        .setText(title)
                        .setImageViewTint(R.id.icon_circle, builder.getThemeColor(R.attr.colorButtonNormal))
                        .setWhen(icon.notification)
                        .setAdaptiveIcon(R.drawable.ic_launcher_foreground)
                        .setSmallIcon(R.drawable.ic_launcher_notification)
                        .setOngoing(true)
                        .setOnlyAlertOnce(true);

                TorrentApplication app = TorrentApplication.from(context);
                if (app.player != null) {
                    Bitmap bm = app.player.getArtwork();
                    if (bm != null) {
                        bm = CacheImagesAdapter.createThumbnail(bm);
                        builder.setViewVisibility(R.id.artwork, View.VISIBLE);
                        builder.setImageViewBitmap(R.id.artwork, bm);
                    } else {
                        builder.setViewVisibility(R.id.artwork, View.GONE);
                    }
                }

                return builder.build();
            }

            @Override
            public void updateIcon() {
                icon.updateIcon(createIntent(getString(R.string.tap_restart), "", false));
            }
        };
        optimization.create();
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d(TAG, "onStartCommand " + intent);

        if (!isRunning()) {
            stopSelf();
            return START_NOT_STICKY;
        }

        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onRestartCommand() {
        BootActivity.createApplication(this);
    }

    @Override
    public void onStartCommand(Intent intent) {
        String a = intent.getAction();
        if (a != null) {
            if (a.equals(UPDATE_NOTIFY)) {
                optimization.icon.updateIcon(intent);
            }
            if (a.equals(SHOW_ACTIVITY)) {
                ProximityShader.closeSystemDialogs(this);
                MainActivity.startActivity(this);
            }
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        sendBroadcast(new Intent(TorrentApplication.SAVE_STATE));

        if (receiver != null) {
            unregisterReceiver(receiver);
            receiver = null;
        }

        headset(false, false);
    }

    void headset(boolean b, final boolean playing) {
        if (b) {
            if (headset == null) {
                final TorrentApplication app = TorrentApplication.from(this);
                headset = new Headset() {
                    @Override
                    public void onPlay() {
                        pause();
                    }

                    @Override
                    public void onPause() {
                        pause();
                    }

                    @Override
                    public void onStop() {
                        app.playerStop();
                    }

                    @Override
                    public void onSkipToNext() {
                        if (app.player == null)
                            return;
                        int i = app.player.getPlaying() + 1;
                        if (i >= app.player.getSize())
                            i = 0;
                        app.player.play(i);
                    }

                    @Override
                    public void onSkipToPrevious() {
                        if (app.player == null)
                            return;
                        int i = app.player.getPlaying() - 1;
                        if (i < 0)
                            i = app.player.getSize() - 1;
                        app.player.play(i);
                    }
                };
                headset.create(this, TorrentReceiver.class);
            }
            headset.setState(playing);
        } else {
            if (headset != null) {
                headset.close();
                headset = null;
            }
        }
    }

    void pause() {
        try {
            pause.send();
        } catch (PendingIntent.CanceledException e) {
            Log.d(TAG, "canceled expcetion", e);
        }
    }
}
